#!/bin/bash
# Build stage
FROM golang:1.19-alpine3.16 AS builder
WORKDIR /app
RUN apk add alpine-sdk
COPY . .

# RUN go mod download
RUN GOOS=linux GOARCH=amd64 go build -o test-jenkins -tags musl

# Run stage
FROM alpine:3.16
WORKDIR /app

RUN apk update && apk add --no-cache git
RUN apk update && apk add --no-cache tzdata
ENV TZ="Asia/Jakarta"
ENV REDIS_EXPIRED_IN_HOURS="168h" 

COPY --from=builder /app/test-jenkins .

EXPOSE 8090
ENTRYPOINT [ "/app/test-jenkins" ]